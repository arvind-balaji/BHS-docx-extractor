import sys
import os
from docx import Document
from PyQt5 import QtCore
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QWidget, QPushButton, QLineEdit, QInputDialog,
QApplication, QFileDialog, QCheckBox, QStatusBar)

class App(QWidget):
	def __init__(self):
		super().__init__()
		self.title = "highlight parser"
		self.left = 20
		self.top = 20
		self.width = 480
		self.height = 360

		self.setWindowTitle(self.title)
		self.setGeometry(self.left, self.top, self.width, self.height)

		self.fileSelectLe = QLineEdit(self)
		self.fileSelectLe.move(self.width/2-110, self.height/2)
		self.fileSelectLe.setPlaceholderText("input file")
		
		self.outFileNameLe = QLineEdit(self)
		self.outFileNameLe.move(self.width/2-110, self.height/2+40)
		self.outFileNameLe.setPlaceholderText("output file")

		self.fileSelectBtn = QPushButton("Select file", self)
		self.fileSelectBtn.move(self.width/2+30, self.height/2-1)
		self.fileSelectBtn.clicked.connect(self.showSelectDialog)
		self.fileSelectBtn.resize(100, 24)

		self.doneBtn = QPushButton("Done", self)
		self.doneBtn.move(self.width-100, self.height-35)
		self.doneBtn.clicked.connect(self.parseFile)
		self.fileSelectBtn.resize(100, 24)
		
		self.highlightCB = QCheckBox("Highlight", self)
		self.highlightCB.setChecked(True)
		self.highlightCB.move(10, 10)
		
		self.underlineCB = QCheckBox("Underline", self)
		self.underlineCB.setChecked(False)
		self.underlineCB.move(10, 35)
		
		self.italicCB = QCheckBox("Italic", self)
		self.italicCB.setChecked(False)
		self.italicCB.move(10, 60)
		
		self.boldCB = QCheckBox("Bold", self)
		self.boldCB.setChecked(False)
		self.boldCB.move(10, 85)

		self.show()

	def showSelectDialog(self):
		filename = QFileDialog.getOpenFileName(self, "Select file", os.getcwd())
		self.fileSelectLe.setText(filename[0])

	def parseFile(self):
		if not self.fileSelectLe.text():
			return None
		if not self.outFileNameLe.text():
			return None

		inp = Document(self.fileSelectLe.text())
		out = Document()
		pars = []

		for par in inp.paragraphs:
			for run in par.runs:
				addPar = 1
				if self.highlightCB.isChecked():
					if run.font.highlight_color is None:
						addPar = 0
				if self.underlineCB.isChecked():
					if not run.underline:
						addPar = 0
				if self.italicCB.isChecked():
					if not run.italic:
						addPar = 0
				if self.boldCB.isChecked():
					if not run.bold:
						addPar = 0
				if addPar:
					pars.append(par)
					break

		for par in pars:
			out.add_paragraph()
			for run in par.runs:
				addRun = 1
				if self.highlightCB.isChecked():
					if run.font.highlight_color is None:
						addRun = 0
				if self.underlineCB.isChecked():
					if not run.underline:
						addRun = 0
				if self.italicCB.isChecked():
					if not run.italic:
						addRun = 0
				if self.boldCB.isChecked():
					if not run.bold:
						addRun = 0
				if addRun:
					out.paragraphs[-1].add_run(run.text+" ")

		out.save(self.outFileNameLe.text()+".docx")


app = QApplication(sys.argv)
ex = App()
sys.exit(app.exec_())
